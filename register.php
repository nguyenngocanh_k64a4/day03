<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Đăng ký</title>
    <style type="text/css">
        .main {
            display: flex;
            justify-content: center;
            align-items: center;
            min-height: 65vh;
        }
        .wrapper {
            width: 25%;
            padding: 50px 40px;
            border: 1.5px solid dodgerblue;


        }

        .field {
            margin-bottom: 25px;
            display: flex;
            align-items: center;
            justify-content: space-between;
        }

        .field__label {
            color: #eeeeee;
            background-color: #3a94e5;
            padding: 12px 10px;
            width: 30%;
        }
        .field__input {
            font-size: 14px;
            font-weight: 400;
            padding: 12px;
            border: 1px solid #3a94e5;
            width: 55%;
        }

        select {
            width: 60%;
            height: 43px;
            border: 1px solid #3a94e5;
        }

        .button {
            display: flex;
            justify-content: center;
            align-items: center;
        }

        .btn-submit {
            font-size: 16px;
            color: #eeeeee;
            background-color: #5fa14a;
            padding: 12px 32px;
            margin-top: 15px;
            border-radius: 10px;
        }

    </style>
</head>
<body>

    <?php
        $sex = array("Nam", "Nữ");
        $faculty = array("MAT"=>"Khoa học máy tính", "KDL"=>"Khoa học vật liệu");
    ?>

    <div class="main">
        <div class="wrapper">
            <form>
                <div class="field">
                    <label for="fullname" class="field__label">Họ và tên</label>
                    <input type="text" id="fullname" class="field__input"/>
                </div>

                <div class="field">
                    <label class="field__label">Giới tính</label>
                    <?php
                        for($i = 0; $i < count($sex); $i++) {
                            echo '<input type="radio" id="' . $sex[$i] . '" name="sex" value="' . $sex[$i] . '">';
                            echo '<label style="user-select: none; cursor: pointer" for="' . $sex[$i] . '">' . $sex[$i] . '</label><br>';
                        }
                    ?>
                </div>

                <div class="field">
                    <label for="faculties" class="field__label">Phân khoa</label>
                    <select id="faculties">
                        <option value="0" selected></option>
                        <?php
                            foreach($faculty as $x => $x_value) {
                                echo '<option value=' . $x_value . '>' . $x_value . '</option>';
                            }
                        ?>
                    </select>
                </div>

                <div class="button">
                    <button type="submit" class="btn-submit">Đăng ký</button>
                </div>
            </form>
        </div>
    </div>

</body>
</html>